class Product:
    def __init__(self, id, name, price, weight,shelf, id_category, balance):
        self.id = id
        self.name = name
        self.price = price
        self.weight = weight
        self.shelf = shelf
        self.id_category = id_category
        self.balance = balance
class Category:
    def __init__(self, id, name):
        self.id = id
        self.name = name
class Deliveries:
    def __init__(self, id, producer, product, date, quantity):
        self.id = id
        self.producer = producer
        self.product = product
        self.date = date
        self.quantity = quantity
class Producer:
    def __init__(self, id, name, adress, bank):
        self.id = id
        self.name = name
        self.adress = adress
        self.bank = bank
