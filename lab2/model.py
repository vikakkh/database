from datetime import datetime

import psycopg2
from entities import Product, Category, Deliveries, Producer

class DataBase:
    def __init__(self, ):
        self.con = None
        self.cur = None

    def connect(self):
        try:
            self.con =  psycopg2.connect(
            database = 'storage_room',
            user = 'postgres',
            password ='vikakh2002',
            host = 'localhost'
            )
            self.cur = self.con.cursor()
        except(Exception, psycopg2.Error):
         print("Connection error!\n")

    def close(self):
        if self.con:
            self.cur.close()
            self.con.close()
            print("PostgreSQL connection is closed")
#product
    def get_product_by_id(self, id):
        self.cur.execute("SELECT * FROM product WHERE id_product = {:d}".format(int(id)))
        productstr = self.cur.fetchall()
        if (len(productstr) == 0):
            print("Incorrect id!")
            return False
        product = Product(int(productstr[0][0]), productstr[0][1], productstr[0][2],
                          productstr[0][3], productstr[0][4], productstr[0][5], productstr[0][6])
        return product

    def insert_product(self, product):
        try:
            self.cur.execute("INSERT INTO public.product(name_product, price, weight_product,"
                             " shelf_life, id_category, product_balance) VALUES(%s,%s,%s,%s,%s,%s) "
                             "RETURNING id_product", (product.name, product.price,
                                                            product.weight, product.shelf,
                                                            product.id_category,product.balance))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Error! can not insert new product in table")
            return
        return id

    def delete_product(self, id):
        try:
            self.cur.execute("DELETE FROM public.product WHERE id_product = {:d}".format(int(id)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Incorrect id")
            return
        return True

    def update_product (self, product):
        try:
            self.cur.execute("UPDATE public.product SET name_product = '{1}' , price = '{2}',"
                             " weight_product = '{3}',shelf_life = '{4}', id_category = '{5}', "
                             "product_balance = '{6}' WHERE id_product= '{0}''".format(product.name,product.price,
                                                                                       product.weight, product.shelf,
                                                                                       product.id_category, product.balance,
                                                                                       int(product.id)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Can not update product")
            return
        return True

#category
    def get_category_by_id(self, id):
        try:
            self.cur.execute("SELECT * FROM category WHERE id_category = {:d}".format(int(id)))
            categorystr = self.cur.fetchall()
            category = Category(int(categorystr[0][0]), categorystr[0][1])
        except(Exception, psycopg2.Error):
            print("Can not get category by id!")
            return
        return category

    def insert_category(self, category):
         try:
             self.cur.execute("INSERT INTO public.category (name_category) VALUES(%s) RETURNING id_category", (category.name))
             id = self.cur.fetchone()[0]
             self.con.commit()
         except(Exception, psycopg2.Error):
            print("Error! can not insert new product in table")
            return
         return id

    def delete_category(self, id):
        try:
            self.cur.execute("DELETE FROM public.category WHERE  id_category= {:d}".format((id)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Error! delete category")
            return
        return True

    def update_category(self, category):
        # try:
            self.cur.execute("UPDATE public.category SET "
                             "name_category = '{1}' WHERE id_category = '{0}'".format((int(category.id)), category.name))
            self.con.commit()
        # except(Exception, psycopg2.Error):
        #     print("Error! update category")
        #     return
            return True

#producer
    def get_producer_by_id(self, id):
        try:
            self.cur.execute("SELECT * FROM producer WHERE id_producer = {:d}".format(int(id)))
            producerstr = self.cur.fetchall()
            producer = Producer(int(producerstr[0][0]),producerstr[0][1],producerstr[0][2],producerstr[0][3] )
        except(Exception, psycopg2.Error):
            print("Can not get producer by id!")
            return
        return producer

    def insert_producer(self, producer):
        try:
            self.cur.execute("INSERT INTO public.producer (name_producer, adress, bank_details) VALUES(%s, %s, %s) "
                             "RETURNING id_producer", (producer.name, producer.adress, producer.bank))
            id = self.cur.fetchone()[0]
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Can not insert producer!")
            return
        return id

    def delete_producer(self, id):
        try:
            self.cur.execute("DELETE FROM public.producer WHERE  id_producer= {:d}".format((id)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Error! delete producer")
            return
        print('\033[0m'+"delete successfully!")
        return True


    def update_producer(self, producer):
        try:
            self.cur.execute("UPDATE public.producer SET "
                             "name_producer='{1}', adress='{2}', bank_details ='{3}'"
                             " WHERE id_producer = '{0}'".format(int(producer.id), producer.name, producer.adress,
                                                                 int(producer.bank)))
            self.con.commit()
        except(Exception, psycopg2.Error):
            print("Error! update producer")
            return
        return True

#delivery
    def get_delivery_by_id(self, id):
        try:
            self.cur.execute("SELECT * FROM deliveries WHERE id_deliveries = {:d}".format(int(id)))
            categorystr = self.cur.fetchall()
            delivery = Deliveries(int(categorystr[0][0]))
        except(Exception, psycopg2.Error):
            print("Can not get delivery by id!")
            return
        return delivery


# sql
    def generate_score(self,number):
        try:
            before_query = datetime.now()
            self.cur.execute("CREATE OR REPLACE FUNCTION add_random_score() RETURNS INTEGER  AS "
                             "$$ DECLARE prod_id INTEGER; DECLARE _score INTEGER; BEGIN "
                             "prod_id = (select id_product from product ORDER BY RANDOM() Limit 1); "
                             "_score = (select trunc(random()*10+1)::int "
                             "from generate_series(1, 2) LIMIT 1); INSERT INTO public.buyers_rating(id_product, score)  "
                             " VALUES(prod_id, _score); RETURN 1; END; $$ LANGUAGE plpgsql; "
                             "select * from  generate_series(1, '{0}') where add_random_score() = 1 ".format(number))
            after_query = datetime.now()
            self.con.commit()
            queryTime = after_query - before_query
            print("Success generate score")
        except(Exception, psycopg2.Error):
            print("Cannot generate score for products")
            return
        return True


    def filter_deliveries_by_date(self, delivery_from, delivery_to,price_from,price_to):
        try:
            before_query = datetime.now()
            self.cur.execute("select id_deliveries,id_producer, deliveries.id_product,date_of_delivery,quantity "
                             "from deliveries "
                             "inner join product on deliveries.id_product = product.id_product "
                             "where date_of_delivery BETWEEN '{0}' AND '{1}' "
                             "AND price BETWEEN '{2}' AND '{3}'".format(delivery_from, delivery_to,price_from,price_to))
            after_query = datetime.now()
            queryTime = after_query - before_query
            print("Time for query: '{0}'\n".format(queryTime))
            _str = self.cur.fetchall()
            deliv = list()
            for i in range(0,len(_str)):
                id=int(_str[i][0])
                producer = int(_str[i][1])
                product = int(_str[i][2])
                date = _str[i][3]
                quantity=int(_str[i][4])
                delv=Deliveries(id, producer, product, date, quantity)
                deliv.append(delv)
        except(Exception, psycopg2.Error):
            print("Cannot filter data")
            return
        return deliv




    def get_all_category_products(self, name_category):
        try:
            products=list()
            self.cur.execute("Select * from product inner join category on product.id_category = category.id_category "
                         "where name_category='{0}'".format(name_category))
            productstr = self.cur.fetchall()
        except(Exception, psycopg2.Error):
            print("Troubles in getting all products certain category!")
            return
        for i in range(0, len(productstr)):
            id = int(productstr[i][0])
            name = productstr[i][1]
            price = int(productstr[i][2])
            weight = productstr[i][3]
            shelf = productstr[i][4]
            id_category = productstr[i][5]
            balance = productstr[i][6]
            prod = Product(id, name, price, weight,shelf,id_category,balance)
            products.append(prod)
        return products

    def get_producer_by_category(self,name_category):
        try:
            producer=list()
            self.cur.execute("Select producer.id_producer, name_producer, adress, bank_details  from producer "
                                 "inner join deliveries on producer.id_producer = deliveries.id_producer "
                                 "inner join product on product.id_product = deliveries.id_product "
                                 "inner join category on category.id_category = product.id_category "
                                 "where name_category = '{0}'".format(name_category))
            productstr = self.cur.fetchall()
            for i in range(0, len(productstr)):
                id_producer = int(productstr[i][0])
                name_producer = productstr[i][1]
                adress = productstr[i][2]
                bank_details = productstr[i][3]
                prod = Producer(id_producer, name_producer, adress, bank_details)
                producer.append(prod)
        except(Exception, psycopg2.Error):
            print("Troubles in getting producer by category!")
            return
        return producer
