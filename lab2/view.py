from entities import Product, Category, Deliveries, Producer

class View:
#menu
    def CUI(self):
        while(True):
            choice = input ("\tMenu: \n1.Generating date \n2.Work with storage_room"
                        "\n3.Find or filter data \n4.Close \nChoice: ")
            if (choice.isdigit()!=True):
                print("Enter correct number to continue work with db")
                continue
            break
        return int(choice)


    def cui_enteties(self):
        while True:
            choice = input("Select table you want to work with:\nProduct \nCategory \nProducer "
                           "\n Exit \nChoice: ")
            if (choice.isdigit() != False):
                print("Write correct name of table!")
                continue
            break
        return str(choice)

    def get_number(self):
        while True:
            number = input("Enter number of generation: ")
            if (number.isdigit() != True):
                print("Incorrect enter! Enter number to continue")
                continue
            break
        return int(number)


    def cui_finding(self):
        while True:
            choice = input("Select number to: \n1.Filtering deliver by date "
                           "\n2.Find products that belong certain category"
                           "\n3.Find producers of products that belong certain category"
                           "\nChoice: ")
            if (choice.isdigit()!=True):
                print("Invorrect input!")
                continue
            break
        return int(choice)

    def filter_delivery(self):
        params = list()
        min_date = input("Enter min date of delivery: ")
        max_date = input("Enter max date of delivery: ")
        min_price = input("Enter min price of product: ")
        max_price = input("Enter max price of product: ")
        params.append(min_date)
        params.append(max_date)
        params.append(min_price)
        params.append(max_price)
        return params

    def print_get_category_products(self):
        while True:
            name = input("Enter name category to get his producer:")
            if (name.isdigit() != False):
                print("Incorrect enter! Enter name to continue")
                continue
            break
        return str(name)

    def print_get_producer_by_category(self):
        while True:
            name = input("Enter name category to get his products:")
            if (name.isdigit() != False):
                print("Incorrect enter! Enter name to continue")
                continue
            break
        return str(name)


    def work_table (self):
        while True:
            choice = input("Enter number: \n1.Create entity \n2. Remove entity \n3. Update entity "
                           "\n4. Input by id \n5. Exit \nChoice: ")
            if(choice.isdigit()!= True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)
#functions
    def get_id(self):
        while(True):
            inp=(input("Enter entity id: "))
            if(inp.isdigit() != True):
                print("Incorrect enter! ")
                continue
            break
        return int(inp)
#tables
    def input_product (self, product):
        print("id: ", product.id)
        print("Name: ", product.name)
        print("Price: ", product.price)
        print("Weight: ", product.weight)
        print("Shelf life: ", product.shelf)
        print("Id category: ", product.id_category)
        print("Balance: ", product.balance)

    def input_all_product (self, products):
        for product in products:
            print("id: ", product.id)
            print("Name: ", product.name)
            print("Price: ", product.price)
            print("Weight: ", product.weight)
            print("Shelf life: ", product.shelf)
            print("Id category: ", product.id_category)
            print("Balance: ", product.balance)

    def input_category(self, category):
        print("Id: ", category.id)
        print("Name: ", category.name)



    def input_producer(self, producer):
        print("Id: ", producer.id)
        print("Name: ", producer.name)
        print("Address: ", producer.adress)
        print("Bank details: ", producer.bank)

    def input_all_producer(self, producers):
        for producer in producers:
            print("Id: ", producer.id)
            print("Name: ", producer.name)
            print("Address: ", producer.adress)
            print("Bank details: ", producer.bank)

    def input_all_deliveries(self,deliveries):
        for delivery in deliveries:
            print("Id: ", delivery.id)
            print("Producer Id: ", delivery.producer)
            print("Product Id: ", delivery.product)
            print("Date of delivery: ", delivery.date)
            print("Quantity: ", delivery.quantity)


#product
    def create_product(self):
        while(True):
            name = input("Enter name of product: ")
            price = input("Enter price: ")
            weight = input("Enter weight of product: ")
            shelf = input("Enter shelf life of product: ")
            category_id = input("Enter the category product belongs to: ")
            balance = input("Enter balance: ")
            if (name.isalpha() != True):
                print("Incorrect enter! Use only letters")
                continue
            elif (price.isdigit() != True or weight.isdigit() != True or shelf.isdigit() != True
            or category_id.isdigit() != True or balance.isdigit() != True):
                print("Pleas enter correct data")
            break
        product = Product(1, name, price, weight, shelf, category_id, balance)
        return product

    def update_product(self, product):
        while(True):
            num = input("Enter number to change: \n1. Name \n2. Price \n3. Weight"
                    "\n4. Shelf life \n5.Category \n6. Balance")
            if (num.isdigit()!=True):
                print("Enter correct input")
                continue
            break
        if (int(num)==1):
            name = input("Enter new Name: ").lstrip()
            name.rstrip()
            product.name = name
        if (int(num)==2):
            while(True):
                price = input("Enter new price: ")
                if(price.isdigit()!= True):
                    print("Input number! ")
                    continue
                break
            product.price = price
        if (int(num) == 3):
            while (True):
                weight = input("Enter new weight: ")
                if (weight.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.weight = weight
        if (int(num) == 4):
            while (True):
                shelf = input("Enter new shelf life of product: ")
                if (shelf.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.shelf = shelf
        if (int(num) == 5):
            while (True):
                category = input("change category: ")
                if (category.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.id_category = category
        if (int(num) == 6):
            while (True):
                bal = input("Enter new balance: ")
                if (bal.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.balance = bal
        return product

#category
    def create_category(self):
        name = input("Enter new category: ")
        category = Category(1, name)
        return category

    def update_category(self, category):
        name = input("Enter new Name: ").lstrip()
        name.rstrip()
        category.name = name
        return category

#producer
    def create_producer(self):
        name = input("Enter name of producer: ")
        adress = input("Enter the address: ")
        bank  = input ("Enter bank details: ")
        producer = Producer(1, name, adress, bank)
        return producer

    def update_producer(self, producer):
        while(True):
            num = input("Enter number to change: \n1. Name \n2. Address \n3. Bank details \nChoice: ")
            if (num.isdigit()!=True):
                print("Enter correct input")
                continue
            break
        if (int(num)==1):
            name = input("Enter new Name: ").lstrip()
            name.rstrip()
            producer.name = name
        if(int(num)==2):
            address = input("Enter new address: ").lstrip()
            address.rstrip()
            producer.adress = address
        if (int(num) == 3):
            while (True):
                bank = input("Enter correct bank details: ")
                if (bank.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            producer.bank = bank
        return producer
