from model import DataBase
from view import View

def main():
    print("vdv")
    view = View()
    db = DataBase()
    db.connect()
    print("\ntry\n")
    while(True):
        choice=view.CUI()
        if choice==1:
            num = view.get_number()
            db.generate_score(num)
        if choice==2:
            while True:
                inp=view.cui_enteties()
                if inp == "product":
                    while True:
                        choice = view.work_table()
                        if choice == 1:
                            product_create_control(db, view)
                        if choice == 2:
                            product_delete_control(db, view)
                        if choice == 3:
                            product_update_control(db, view)
                        if choice == 4:
                            product_get_control(db, view)
                if inp == "category":
                    while True:
                        choice = view.work_table()
                        if choice == 1:
                            category_create_control(db, view)
                        if choice == 2:
                            category_delete_control(db, view)
                        if choice == 3:
                            category_update_control(db, view)
                        if choice == 4:
                            category_get_control(db, view)
                if inp == "producer":
                    while True:
                        choice = view.work_table()
                        if choice == 1:
                            producer_create_control(db, view)
                        if choice == 2:
                            producer_delete_control(db, view)
                        if choice == 3:
                            producer_update_control(db, view)
                        if choice == 4:
                            producer_get_control(db, view)
                if inp == "exit":
                    break
                else:
                    print("Please enter correct name of table to work with: ")
                    continue
        if choice ==3:
            choice=view.cui_finding()
            if choice==1:
                str = view.filter_delivery()
                deliveries = db.filter_deliveries_by_date(str[0], str[1], str[2], str[3])
                view.input_all_deliveries(deliveries)
            if choice==2:
                name = view.print_get_category_products()
                product = db.get_all_category_products(name)
                view.input_all_product(product)
            if choice==3:
                name = view.print_get_producer_by_category()
                producer = db.get_producer_by_category(name)
                view.input_all_producer(producer)
        if choice == 4:
            db.close()
            break




#product
def product_get_control(db, view):
    id = view.get_id()
    product = db.get_product_by_id(id)
    view.input_product(product)

def product_create_control(db, view):
    product = view.create_product()
    product.id = db.insert_product(product)
    view.input_product(product)

def product_delete_control(db, view):
    id=view.get_id()
    product = db.get_product_by_id(id)
    view.input_product(product)
    db.delete_product(id)

def product_update_control(db, view):
    id = view.get_id()
    product = db.get_product_by_id(id)
    view.input_product(product)
    prod = view.update_product(product)
    db.update_product(prod)
    view.input_product(prod)


# category
def category_get_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    view.input_category(category)

def category_create_control(db, view):
    category = view.create_category()
    category.id = db.insert_category(category)
    view.input_category(category)

def category_delete_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    view.input_category(category)
    db.delete_category(id)

def category_update_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    view.input_category(category)
    cat = view.update_category(category)
    db.update_category(cat)
    view.input_category(cat)

#producer
def producer_get_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    view.input_producer(producer)

def producer_create_control(db, view):
    producer = view.create_producer()
    producer.id = db.insert_producer(producer)
    view.input_producer(producer)

def producer_delete_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    view.input_producer(producer)
    db.delete_producer(id)

def producer_update_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    view.input_producer(producer)
    pro= view.update_producer(producer)
    db.update_producer(pro)
    view.input_producer(pro)

main()