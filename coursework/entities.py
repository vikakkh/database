import psycopg2
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy import Table, MetaData
from sqlalchemy import ForeignKey



Base = declarative_base()

class Car(Base):
    __tablename__ = 'car'
    id_car=Column(Integer, primary_key=True)
    id_owner_car= Column(Integer, ForeignKey('owner_car.id_owner_car'))
    _id_owner_car =relationship("Owner_car", backref = "car")
    car_numbers = Column(String(60))
    car_model = Column(String(60))
    car_color = Column(String(60))
    car_typ = Column(String(60))
    id_notice =Column(Integer, ForeignKey('notice.id_notice'))
    _id_notice = relationship("Notice", backref="car")
    id_gai=Column(Integer)
    def __init__(self,owner_car, numbers, model, color,typ, notice, gai):
        self.id_owner_car = owner_car
        self.car_numbers = numbers
        self.car_model = model
        self.car_color = color
        self.car_typ=typ
        self.id_notice = notice
        self.id_gai = gai


class Notice(Base):
    __tablename__ = 'notice'
    id_notice = Column(Integer, primary_key=True)
    engine_number = Column(Integer)
    date_of_inspection = Column(Date)
    next_date_of_inspection = Column(Date)
    payment_date = Column(Date)
    next_payment_date = Column(Date)
    payment = Column(Integer)
    inspection = Column(Integer)
    sum_debt_notice = Column(Integer)

    def __init__(self, number, date_inspection, next_date_inspection,
                 date_payment, next_date_payment, pay, inspec, sum):
        self.engine_number = number
        self.date_of_inspection = date_inspection
        self.next_date_of_inspection = next_date_inspection
        self.payment_date = date_payment
        self.next_payment_date = next_date_payment
        self.payment = pay
        self.inspection = inspec
        self.sum_debt_notice = sum

class Owner_car(Base):
        __tablename__ = 'owner_car'
        id_owner_car = Column(Integer, primary_key=True)
        category_of_driver_license = Column(String)
        name_secondname = Column(String)
        dateofbirthday = Column(Date)
        def __init__(self, license, name_surname, birthday):
            self.category_of_driver_license = license
            self.name_secondname = name_surname
            self.dateofbirthday = birthday

class Gai(Base):
    __tablename__='gai'
    id_gai=Column(Integer,primary_key=True)
    adress=Column(String)
    sum_debt=Column(Integer)
    def __init__(self, address, summ):
        self.adress = address
        self.sum_debt=summ




