from entities import Car, Notice, Owner_car
from datetime import datetime
from colr import color



class View:
#menu
    def cui(self):
        while True:
            choice = input (color("MENU: "
                           "\n 1. Work with database"
                           "\n 2. Filter data"
                           "\n 3. Statistic date"
                           "\n 4. Close database"
                           "\n Your choice: ", fore='B2B2FF', style='bright'))
            if (choice.isdigit() != True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)

    def cui_statistic(self):
        while True:
            choice=input(color("Select data for static analysis:"
                         "\n1. Sum debt all owners in database input by owners"
                         "\n2. Analise payment all owners in database"
                         "\n3. Analise amount of people by category"
                         "\n4. Exit"
                         "\n Your choice: ",fore= '00FFFF', style='bold'))
            if (choice.isdigit()!=True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)

    def cui_enteties(self):
        while True:
            choice = input(color("MENU: "
                                 "\n 1. Owner"
                                 "\n 2. Car"
                                 "\n 3. Notice"
                                 "\n 4. Gai"
                                 "\n 5. Exit"
                                 "\n Your choice: ", fore='FF1493', style='bright'))
            if (choice.isdigit() != True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)

    def cui_generate(self):
        while True:
            choice = input(color("Enter table you want to generate: \n1. Car"
                           "\n2. Notice "
                           "\n3. Owner "
                           "\n4. Exit"
                           "\n Your choice: ",fore='E6E6FA', style='bold'))
            if (choice.isdigit() != True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)


    def generate_amount(self):
        while True:
            number=input(color("Enter amount of generation records in the table: ",fore='E6E6FA', style='bold'))
            if (number.isdigit() != True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(number)


    def cui_filter(self):
        while True:
            choice=input(color("Enter number \n1. Get owner by car info"
                         "\n2. Get owners by certain category of driver license and date of inspection "
                         "\n3. Get cars by owner who has sum debt more than ..."
                         "\n4. Exit"
                         "\n Your choice: ",fore='FFFF00', style='bold'))
            if (choice.isdigit() != True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)



    #FOR TABLES Car Notice Owner_car
    def work_table (self):
        while True:
            choice = input(color("Enter number: \n1. Create entity \n2. Remove entity \n3. Update entity "
                           "\n4. Input by id "
                                 "\n5. Input all"
                                 "\n6. Generate"
                                 "\n7. Exit \nChoice: ",fore='FFEFD5', style='bold'))
            if(choice.isdigit()!= True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)

# functions
    def get_id(self):
        while (True):
            inp = (input("Enter entity id: "))
            if (inp.isdigit() != True):
                print("Incorrect enter! ")
                continue
            break
        return int(inp)

#INPUTS
    def input_car (self, car):
        print("id: ", car.id_car)
        print("id of Owner: ", car.id_owner_car)
        print("\tInformation about car: ")
        print("Numbers: ", car.car_numbers)
        print("Model: ", car.car_model)
        print("Color: ", car.car_color)
        print("Type: ", car.car_typ)
        print("\tNotice: ")
        print("Notice: ", car.id_notice)
        print("GAI: ", car.id_gai)


    def input_all_cars(self, cars):
        print(color("[Id_owner, Number , Model, Color, Type, Id_notice, Gai]", fore='red'))
        for car in cars:
            print("[", car.id_owner_car,",",car.car_numbers,",",car.car_model,",",car.car_color,","
                  ,car.car_typ,",",car.id_notice,",",car.id_gai,"]")

    def input_owner (self, owner):
        print("id: ", owner.id_owner_car)
        print("Name Surname: ", owner.name_secondname)
        print("BirthDay ", owner.dateofbirthday)
        print("Driver license: ", owner.category_of_driver_license)

    def input_all_owners(self, owners):
        print(color("[B-day, Drive license, Name Secondame]",fore='red'))
        for owner in owners:
            print("[",owner.name_secondname,",",owner.dateofbirthday,",",owner.category_of_driver_license,"]")

    def input_notice (self, notice):
        print("id: ", notice.id_notice)
        print("Engine Number: ", notice.engine_number)
        print("Inspection date: ", notice.date_of_inspection)
        print("Next Inspection date: ", notice.next_date_of_inspection)
        print("Payment date: ", notice.payment_date)
        print("Next Payment day: ", notice.next_payment_date)
        print("Payment: ", notice.payment)
        print("Inspection: ", notice.inspection)
        print("Sum debt: ", notice.sum_debt_notice)

    def input_all_notices (self, notices):
        print(color("[Id, Engine number, Date of inspection, Next date, Payment date, Next date, Payment, Inspection]",fore='red'))
        for notice in notices:
            print("[",notice.id_notice, ",", notice.engine_number, ",",notice.date_of_inspection, ",",notice.next_date_of_inspection,
                  ",", notice.payment_date, "," ,notice.next_payment_date, ",",notice.payment, ",",notice.inspection,
                  ",",notice.sum_debt_notice,"]")



    def input_GAI(self, gais):
        for gai in gais:
            print("Id: ", gai.id_gai)
            print("Address: ", gai.adress)
            print("Sum debt: ", gai.sum_debt)

#CAR
    def create_car(self):
        while(True):
            id_owner_car=input("Enter id of owner: ")
            numbers = input("Enter car numbers: ")
            model = self.get_model()
            color = self.get_color()
            type = self.get_type()
            notice_id = input("Enter notice_id: ")
            gai_id= 1
            break
        car = Car(id_owner_car, numbers, model, color, type, notice_id,gai_id)
        return car

    def update_car(self, car):
        while(True):
            num = input("Enter number to change: \n1. Id of owner \n2. Car number \n3. Car model"
                    "\n4. Car color \n5. Car type \n6. Notice id"
                        "\n--:")
            if (num.isdigit()!=True):
                print("Enter correct input")
                continue
            break
        if (int(num)==1):
            while (True):
                owner = input("Enter new Id of owner: ")
                if (owner.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            car.id_owner_car = owner
        if (int(num)==2):
            numbers = input("Enter new numbers: ").lstrip()
            numbers.rstrip()
            car.car_numbers = numbers
        if (int(num) == 3):
            model = self.get_model()
            car.car_model=model
        if (int(num) == 4):
            color = self.get_color()
            car.car_color = color
        if (int(num) == 5):
            type = self.get_type()
            car.car_typ = type
        if (int(num) == 6):
            while (True):
                notice = input("change id of notice: ")
                if (notice.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            car.id_notice = notice
        return car

# OWNER
    def create_owner(self):
        while (True):
            license =self.get_license()
            name = input("Enter name & surname: ").lstrip()
            name.rstrip()
            date = self.get_data()
            break
        owner = Owner_car(license, name, date)
        return owner

    def update_owner(self, owner):
        while (True):
            num = input("Enter number to change: \n1. Name \n2. Date"
                        "\n3. Category of driver license"
                            "\n--:")
            if (num.isdigit() != True):
                print("Enter correct input")
                continue
            break
        if (int(num) == 1):
            name = input("Enter new Name & Surname: ").lstrip()
            name.rstrip()
            owner.name_secondname = name
        if (int(num) == 2):
            date=self.get_data()
            owner.dateofbirthday=date
        if(int(num)==3):
            cate=self.get_license()
            owner.category_of_driver_license=cate
        return owner


        # NOTICE
    def create_notice(self):
        while (True):
            number = input("Enter engine number of car: ")
            inspection_date=self.get_data()
            next_inspection_date = self.get_data()
            payment_date = self.get_data()
            next_payment_date = self.get_data()
            payment = input("Enter payment : ")
            inspec = input("Enter inspection: ")
            sum =input("Enter sum debt : ")
            break
        notice = Notice(number, inspection_date, next_inspection_date,
                      payment_date, next_payment_date, payment,inspec,sum)
        return notice


    def update_notice(self, notice):
        while (True):
            num = input("Enter number to change: \n1. Engine number \n2. Inspection date "
                    "\n3. Next inspection date"
                    "\n4. Payment date \n5.Next payment date "
                    "\n6. Payment \n7. Inspection \n8.Sum debt"
                    "\n--:")
            if (num.isdigit() != True):
                print("Enter correct input")
                continue
            break
        if (int(num) == 1):
            while (True):
                number = input("Enter new number: ")
                if (number.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            notice.engine_number = number
        if (int(num) == 2):
            date = self.get_data()
            notice.date_of_inspection = date
        if (int(num) == 3):
            date = self.get_data()
            notice.next_date_of_inspection = date
        if (int(num) == 4):
            date = self.get_data()
            notice.payment_date = date
        if (int(num) == 5):
            date =self.get_data()
            notice.next_payment_date = date
        if (int(num) == 6):
            while (True):
                pay = input("Enter new payment: ")
                if (pay.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            notice.payment = pay
        if (int(num) == 7):
            while (True):
                insp = input("Enter new inspection: ")
                if (insp.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            notice.inspection = insp
        if (int(num) == 8):
            while (True):
                sum = input("Enter new sum dbt: ")
                if (sum.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            notice.sum_debt_notice = sum
        return notice


# in

    def filter_owner_by_info_car(self):
        params=list()
        model=self.get_model()
        color=self.get_color()
        type = self.get_type()
        params.append(model)
        params.append(color)
        params.append(type)
        return params

    def filter_owner_by_category_dateofinspection(self):
        params=list()
        category=self.get_license()
        date1=self.get_data()
        date2=self.get_data()
        params.append(category)
        params.append(date1)
        params.append(date2)
        return params

    def filter_car_by_sumdebt(self):
        while True:
            sum=input("Input sum debt: ")
            if (sum.isdigit()==False):
                print("Incorrect input. Try again")
                continue
            return int(sum)



    def get_data(self):
        while True:
            try:
                date_entry = input('Enter a date (i.e. 2017-07-01): ')
                year, month, day = map(int, date_entry.split('-'))
                date = datetime(year, month, day)
                return date
            except ValueError as e:
                print(e)
                continue
            break

    def get_license(self):
        while True:
            category=input("Category of driver license: ").upper()
            if (category!="A" and category!= "B" and category!="C"
                and category!="T" and category!="D" and category!="E"):
                print("Please enter right category: ")
                continue
            break
        return  category

    def get_model(self):
        while True:
            model = input("input model of car: ")
            if (model != "Mers" and model != "BMW" and model != "Lada"
                    and model != "KIA" and model != "VolksWagen" and model != "Skoda"
                    and model != "Porshe" and model != "Toyota" and model != "Lexus"
                    and model != "Tesla" and model != "Rang Rover" and model != "Opel"
                    and model != "Ford" and model != "Mazda" and model != "Pegout"
                    and model != "Jaguar" and model != "Shevrolet" and model != "Hundai"
                    and model != "Honda" and model != "Renault" and model != "Audi"            ):
                print("Please enter correct model of car: ")
                continue
            break
        return model


    def get_color(self):
        while True:
            color = input("input color of car: ")
            if (color != "yellow" and color != "green" and color != "white"
                    and color != "blue" and color != "gray" and color != "brown"
                    and color != "black" ):
                print("Please enter correct color of car: ")
                continue
            break
        return color

    def get_type(self):
        while True:
            type = input("input tupe of car: ")
            if (type != "sedan" and type != "family" and type != "octavia"
                    and type != "cayen" and type != "x3" and type != "universal"
                    and type != "superb"  and type != "truc" and type != "camara" ):
                print("Please enter correct type of car: ")
                continue
            break
        return type