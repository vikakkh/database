from orm import DataBase
from view import View
import colr

def main():
    view = View()
    db = DataBase()
    db.connect()
    while True:
        menu = view.cui()
        if menu ==1:
            entity=view.cui_enteties()
            if entity==1:
                while True:
                    work=view.work_table()
                    if work==1:
                        owner_create_control(db, view)
                    if work==2:
                        owner_delete_control(db, view)
                    if work==3:
                        owner_update_control(db, view)
                    if work==4:
                        owner_get_contol(db, view)
                    if work==5:
                        owners=db.get_all_owners()
                        view.input_all_owners(owners)
                    if work==6:
                        amount = view.generate_amount()
                        db.generate_owner_car(amount)
                    if work==7:
                        break
            if entity==2:
                while True:
                    work = view.work_table()
                    if work==1:
                        car_create_control(db, view)
                    if work==2:
                        car_delete_control(db, view)
                    if work==3:
                        car_update_control(db, view)
                    if work==4:
                        car_get_contol(db, view)
                    if work==5:
                        cars=db.get_all_cars()
                        view.input_all_cars(cars)
                    if work==6:
                        amount = view.generate_amount()
                        db.generate_car(amount)
                    if work==7:
                        break
            if entity==3:
                while True:
                    work = view.work_table()
                    if work==1:
                        notice_create_control(db, view)
                    if work==2:
                        notice_delete_control(db, view)
                    if work==3:
                        notice_update_control(db, view)
                    if work==4:
                        notice_get_contol(db, view)
                    if work==5:
                        notice=db.get_all_notice()
                        view.input_all_notices(notice)
                    if work==6:
                        amount = view.generate_amount()
                        db.generate_notice(amount)
                    if work==7:
                        break
            if entity==4:
                gai=db.get_all_gai()
                view.input_GAI(gai)
            if entity==5:
                break
        if menu==2:
            while True:
                filter=view.cui_filter()
                if filter==1:
                    params = view.filter_owner_by_info_car()
                    own = db.filter_owner_by_info_car(params[0], params[1], params[2])
                    view.input_all_owners(own)
                if filter==2:
                    params = view.filter_owner_by_category_dateofinspection()
                    own = db.filter_owner_by_category_dateofinspection(params[0], params[1], params[2])
                    view.input_all_owners(own)
                if filter==3:
                    params = view.filter_car_by_sumdebt()
                    num = db.filter_car_by_sumdebt(params)
                    view.input_all_cars(num)
                if filter == 4:
                    break
        if menu==3:
            while True:
                statistic=view.cui_statistic()
                if statistic==1:
                    db.analiz_sum_debt_by_car_numbers()
                if statistic==2:
                    db.analiz_payment_by_name()
                if statistic==3:
                    db.analiz_inspection_by_name()
                if statistic==4:
                    break
        if menu==4:
            db.close()



#CAR
def car_get_contol(db,view):
    id = view.get_id()
    car = db.get_car_by_id(id)
    if (car == False):
        return
    view.input_car(car)

def car_create_control(db, view):
    amount=db.check_cars()
    print(amount)
    car = view.create_car()
    car.id = db.insert_car(car)
    amount1=db.check_cars()
    print(amount1)



def car_update_control(db, view):
    id = view.get_id()
    car = db.get_car_by_id(id)
    view.input_car(car)
    car2 = view.update_car(car)
    db.update_car(car2)
    view.input_car(car2)

def car_delete_control(db, view):
    amount = db.check_cars()
    print(amount)
    id = view.get_id()
    car = db.get_car_by_id(id)
    db.delete_car(id)
    amount1 = db.check_cars()
    print(amount1)


#NOTICE
def notice_get_contol(db,view):
    id = view.get_id()
    notice = db.get_notice_by_id(id)
    if (notice == False):
        return
    view.input_notice(notice)

def notice_create_control(db, view):
    amount = db.check_notice()
    print(amount)
    notice = view.create_notice()
    notice.id = db.insert_notice(notice)
    view.input_notice(notice)
    amount1 = db.check_notice()
    print(amount1)


def notice_update_control(db, view):
    id = view.get_id()
    notice = db.get_notice_by_id(id)
    view.input_notice(notice)
    notice2 = view.update_notice(notice)
    db.update_notice(notice2)
    view.input_notice(notice2)

def notice_delete_control(db, view):
    amount = db.check_notice()
    print(amount)
    id = view.get_id()
    notice = db.get_notice_by_id(id)
    db.delete_notice(id)
    amount1 = db.check_notice()
    print(amount1)

# OWNER
def owner_get_contol(db,view):
    id = view.get_id()
    owner = db.get_owner_by_id(id)
    if (owner == False):
        return
    view.input_owner(owner)

def owner_create_control(db, view):
    amount = db.check_owner_cars()
    print(amount)
    owner = view.create_owner()
    owner.id = db.insert_owner(owner)
    view.input_owner(owner)
    amount1 = db.check_owner_cars()
    print(amount1)


def owner_update_control(db, view):
    id = view.get_id()
    owner = db.get_owner_by_id(id)
    view.input_owner(owner)
    owner2 = view.update_owner(owner)
    db.update_owner(owner2)
    view.input_owner(owner2)

def owner_delete_control(db, view):
    amount = db.check_owner_cars()
    print(amount)
    id = view.get_id()
    owner = db.get_owner_by_id(id)
    view.input_owner(owner)
    db.delete_owner(id)
    amount1 = db.check_owner_cars()
    print(amount1)



main()