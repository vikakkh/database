from entities import Car, Notice, Owner_car, Gai
from sqlalchemy import create_engine, exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import pandas as pd
import matplotlib.pyplot as pplt
from colr import color

Base = declarative_base()
class DataBase:
    def connect(self):
        self.engine = create_engine('postgresql://postgres:vikakh2002@localhost:5432/coursework')
        self.Session = sessionmaker(bind=self.engine)
        self.session= self.Session()

    def close(self):
        self.session.close()

    # -----CAR-----
    def get_car_by_id(self,id):
        try:
            car = self.session.query(Car).get(id)
            self.session.commit()
        except exc.SQLAlchemyError:
            print(f"There is no car with id {id}")
            return False
        return car



    def insert_car(self,car):
        try:
            self.session.add(car)
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot insert new record in table")
            return False
        return car.id_car

    def check_cars(self):
        rows=self.session.query(Car).count()
        return rows


    def update_car(self, car):
        try:
            update_car = self.get_car_by_id(car.id_car)
            update_car.id_owner_car = car.id_owner_car
            update_car.car_numbers = car.car_numbers
            update_car.car_model = car.car_model
            update_car.car_color = car.car_color
            update_car.car_typ = car.car_typ
            update_car.id_notice = car.id_notice
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot update new record in table")
            return False
        return True

    def delete_car(self,id):
        try:
            delete_car=self.get_car_by_id(id)
            self.session.delete(delete_car)
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot delete record in table")
            return False
        return True

    def get_all_cars(self):
        try:
            cars = self.session.query(Car).all()
        except exc.SQLAlchemyError:
            print("Cannot get all cars")
            return False
        return cars

# --------OWNER----------
    def get_owner_by_id(self,id):
        try:
            owner=self.session.query(Owner_car).get(id)
        except  exc.SQLAlchemyError:
            print("There is no owner with such id")
            return False
        return owner

    def check_owner_cars(self):
        rows=self.session.query(Owner_car).count()
        return rows


    def insert_owner(self,owner):
        try:
            self.session.add(owner)
            self.session.commit()
        except  exc.SQLAlchemyError:
            print("Cannot inser owner in table")
            return False
        return owner.id_owner_car

    def update_owner(self, owner):
        try:
            update_owner = self.get_owner_by_id(owner.id_owner_car)
            update_owner.name_secondname = owner.name_secondname
            update_owner.dateofbirthday= owner.dateofbirthday
            update_owner.category_of_driver_license=owner.category_of_driver_license
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot update new record in table")
            return False
        return True

    def delete_owner (self,id):
        try:
            delete_owner=self.get_owner_by_id(id)
            self.session.delete(delete_owner)
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot delete new record in table")
            return False
        return True


    def get_all_owners(self):
        try:
            owners = self.session.query(Owner_car).all()
        except exc.SQLAlchemyError:
            print("Cannot get all owners")
            return False
        return owners

# ----------------NOTICE----------
    def get_notice_by_id(self, id):
        notice = self.session.query(Notice).get(id)
        return notice

    def check_notice(self):
        rows=self.session.query(Notice).count()
        return rows

    def insert_notice(self, notice):
        try:
            self.session.add(notice)
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot insert new record in table")
            return False
        return notice.id_notice

    def update_notice(self, notice):
        try:
            update_notice = self.get_notice_by_id(notice.id_notice)
            update_notice.engine_number = notice.engine_number
            update_notice.date_of_inspection = notice.date_of_inspection
            update_notice.next_date_of_inspection = notice.next_date_of_inspection
            update_notice.payment_date = notice.payment_date
            update_notice.next_payment_date = notice.next_payment_date
            update_notice.payment = notice.payment
            update_notice.inspection = notice.inspection
            update_notice.sum_debt_notice = notice.sum_debt_notice
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot update new record in table")
            return False
        return True

    def delete_notice(self, id):
        try:
            delete_notice = self.get_notice_by_id(id)
            self.session.delete(delete_notice)
            self.session.commit()
        except exc.SQLAlchemyError:
            print("Cannot delete new record in table")
            return False
        return True

    def get_all_notice(self):
        try:
            notices = self.session.query(Notice).all()
        except exc.SQLAlchemyError:
            print("Cannot get all notices")
            return False
        return notices

# ------------GAI-------------
    def get_all_gai(self):
        gai=self.session.query(Gai).all()
        return gai


# ----------FILTER------------
    def filter_owner_by_info_car(self,car_model,car_color,car_typ):
        try:
             owners=list()
             result =self.session.execute("SELECT owner_car.id_owner_car, name_secondname, dateOfBirthday, category_of_driver_license "
                                          "from Owner_car "
                           "inner join Car on Car.ID_Owner_car = Owner_car.ID_Owner_car "
                           "inner join Notice on Car.Id_Notice = Notice.Id_Notice "
                           "where Car_Model = '{0}' and Car_Color = '{1}' and Car_Typ = '{2}' "
                           "order by name_secondname".format(car_model,car_color,car_typ))
             ownerstr = result.fetchall()
             for i in range(0,len(ownerstr)):
                 license = ownerstr[i][1]
                 name = ownerstr[i][2]
                 b_day = ownerstr[i][3]
                 owner=Owner_car(license,name,b_day)
                 owners.append(owner)
        except exc.SQLAlchemyError:
            print("Cannot do this request")
            return False
        return owners

    def filter_owner_by_category_dateofinspection(self,category,date1,date2):
        try:
             owners=list()
             result =self.session.execute("Select owner_car.id_owner_car, name_secondname,dateOfBirthday,category_of_driver_license "
                                          "from Car "
                                          "inner join Notice on Car.Id_Notice = Notice.ID_Notice "
                                          "inner join Owner_car on Car.ID_Owner_car = Owner_car.ID_Owner_car "
                                          "where category_of_driver_license='{0}' and Date_of_inspection between '{1}' "
                                          "and '{2}'".format(category,date1,date2))
             ownerstr = result.fetchall()
             for i in range(0,len(ownerstr)):
                 license = ownerstr[i][1]
                 name = ownerstr[i][2]
                 b_day = ownerstr[i][3]
                 owner=Owner_car(license,name,b_day)
                 owners.append(owner)
        except exc.SQLAlchemyError:
             print("Cannot do this request")
             return False
        return owners

    def filter_car_by_sumdebt(self,sum):
        try:
            car=list()
            result = self.session.execute("Select id_car, car.id_owner_car,car_numbers, car_model,"
                                          " car_color, car_typ, car.id_notice, id_gai"
                                          " from car inner join Notice on Car.Id_Notice = Notice.ID_Notice "
                                          "inner join Owner_car on Car.ID_Owner_car = Owner_car.ID_Owner_car"
                                          " where sum_debt_Notice > '{0}'".format(sum))
            carstr=result.fetchall()
            for i in range(0, len(carstr)):
                id_owner = int(carstr[i][1])
                numbers=carstr[i][2]
                model=carstr[i][3]
                color=carstr[i][4]
                typ=carstr[i][5]
                notice= int(carstr[i][6])
                gai= int(carstr[i][7])
                cars=Car(id_owner,numbers,model,color,typ,notice,gai)
                car.append(cars)
        except exc.SQLAlchemyError:
            print("Cannot do this request")
            return False
        return car

    def analiz_sum_debt_by_car_numbers(self):
        df = pd.read_sql_table('car', self.engine)
        df2 = pd.read_sql_table('notice', self.engine)
        df3 = pd.read_sql_table('owner_car', self.engine)
        df_3 = pd.merge(df, df2, how='inner', on='id_notice')
        df_4 = pd.merge(df_3, df3, how='inner', on='id_owner_car')
        new_df_2 = df_4.groupby('name_secondname')['sum_debt_notice'].mean()
        new_df_2.plot.bar()
        pplt.show()

    def analiz_payment_by_name(self):
        df = pd.read_sql_table('car', self.engine)
        df2 = pd.read_sql_table('notice', self.engine)
        df3 = pd.read_sql_table('owner_car', self.engine)
        df_3 = pd.merge(df, df2, how='inner', on='id_notice')
        df_4 = pd.merge(df_3, df3, how='inner', on='id_owner_car')
        new_df_2 = df_4.groupby('name_secondname')['payment'].mean()
        new_df_2.plot.bar()
        pplt.show()


    def analiz_inspection_by_name(self):
        df = pd.read_sql_table('car', self.engine)
        df2 = pd.read_sql_table('notice', self.engine)
        df3 = pd.read_sql_table('owner_car', self.engine)
        df_3 = pd.merge(df, df2, how='inner', on='id_notice')
        df_4 = pd.merge(df_3, df3, how='inner', on='id_owner_car')
        new_df_2 = df_4.groupby('name_secondname')['inspection'].mean()
        new_df_2.plot.bar()
        pplt.show()



    def generate_notice(self, count):
        rows = self.session.query(Notice).count()
        print(color("Before generation in table Notice amount of rows:", fore='B2B2FF'))
        print(rows)
        self.session.execute(f"insert into notice (engine_number, date_of_inspection, next_date_of_inspection, payment_date,next_payment_date, "
                       f"  payment, inspection, sum_debt_notice)"
                       f" select rnd.engine_number, rnd.date_of_inspection, rnd.next_date_of_inspection, rnd.payment_date,"
                       f" rnd.next_payment_date, rnd.payment, rnd.inspection, rnd.sum_debt_notice"
                       f" from (select trunc(random()*(99999999-10000000)+99999999) as engine_number,"
                       f" timestamp'2018-01-01'+random()*(timestamp'2018-01-01'-timestamp'2017-01-01') as date_of_inspection,"
                       f" timestamp'2019-01-01'+random()*(timestamp'2019-01-01'-timestamp'2018-01-01') as next_date_of_inspection,"
                       f" timestamp'2018-01-01'+random()*(timestamp'2018-01-01'-timestamp'2017-01-01') as payment_date,"
                       f" timestamp'2019-01-01'+random()*(timestamp'2019-01-01'-timestamp'2018-01-01') as next_payment_date, "
                       f" trunc(random()*(10000-100)+10000) as payment,"
                       f" trunc(random()*(10000-100)+10000) as inspection,"
                       f" trunc(random()*(10000-100)+10000) as sum_debt_notice from generate_series(1,{count})) as rnd")
        self.session.commit()
        print(f"Generate {count} records in table Notice")
        print(color("After generation:", fore='B2B2FF'))
        rows_after = self.session.query(Notice).count()
        print(rows_after)
        return True

    def generate_car(self, count):
        if (self.check()==True):
            rows = self.session.query(Car).count()
            print(color("Before generation in table Car amount of rows:", fore='B2B2FF'))
            print(rows)
            self.session.execute(f"insert into car (id_owner_car, car_numbers, car_model, car_color, car_typ, id_notice, id_gai)"
                                 f" select rnd.id_owner_car, rnd.car_numbers, rnd.car_model, rnd.car_color, rnd.car_typ,"
                                 f" rnd.id_notice, rnd.id_gai from (select ( "
                                 f"(array['AA','KA','BA','BI','BE','BO','BT','AX','AP','AE','AI','BH','BB','CA'])[floor(random()*14+1)]"
                                 f" ||(RANDOM() * 9)::INT||(RANDOM() * 9)::INT||(RANDOM() * 9)::INT||(RANDOM() * 9)::INT"
                                 f" ||(array['A','B','C','E','T','I','O','P','A','H','K'])[floor(random()*11+1)] "
                                 f"||(array['A','B','C','E','T','I','O','P','A','H','K'])[floor(random()*11+1)])"
                                 f" as car_numbers,(array['Mers','BMW','Lada','KIA','VolksWagen','Skoda','Porshe','Toyota', 'Lexus','Tesla',"
                                 f" 'Rang Rover','Opel','Ford','Mazda','Pegout','Jaguar','Shevrolet','Hundai','Honda',"
                                 f" 'Renault','Audi', 'Lanos','Citroen'])"
                                 f"[floor(random()*23+1)] as car_model,(array['blue','gray','brown','yellow','green','white',"
                                 f" 'black'])[floor(random()*7+1)] as car_color,(array['sedan','family','octavia','cayen','x3',"
                                 f" 'universal','superb','truc','camara'])[floor(random()*9+1)] as car_typ,"
                                 f" notice.id_notice as id_notice,owner_car.id_owner_car as id_owner_car,gai.id_gai "
                                 f"as id_gai from notice, owner_car, gai) as rnd left join car "
                                 f" on(rnd.id_owner_car=car.id_owner_car and rnd.id_notice=car.id_notice and "
                                 f" rnd.id_gai=car.id_gai) where car.id_car is NULL order by random() limit {count}")
            self.session.commit()
            print(f"Generate {count} records in table Car")
            print(color("After generation:", fore='B2B2FF'))
            rows_after = self.session.query(Car).count()
            print(rows_after)
            return True
        else:
            return False

    def generate_owner_car(self, count):
        rows=self.session.query(Owner_car).count()
        print(color("Before generation in table Owner_car amount of rows:" , fore='B2B2FF'))
        print (rows)
        self.session.execute(f"insert into owner_car (category_of_driver_license, name_secondname, dateofbirthday) "
                             f"select rnd.category_of_driver_license, rnd.name_secondname, rnd.dateofbirthday "
                             f" from (select  ( (array['John','Vika','Anastasia','Kate','Veronika','Vova', "
                             f"'Zhenya','Andrew','Maksym','Ihor','Egor','Stas','Ilya','Alexsandr','Lilia','Vitalina', "
							 f"'Daniil','Oleg','Sergey','Viltaliy','Bohdan','Dasha','Daryna','Vanya','Lisa','Marta', "
                             f"'Jaroslav','Lera','Ruslan','Robert','Alina', 'Gabriella','Denys','Paulina','Sonya', "
                             f"'Maria','Diana','Dmitriy','Andro','Olya','Myroslava','Oleksiy'])[floor(random()*42+1)] "
                             f"||(array[' Ray',' Edwards',' Phillips',' Holland',' Chandler',' Burke',' Mccarthy', "
                             f"' Carr',' Campbell',' Dunn',' Spencer',' Hudson',' Neal',' Long',' Herrera',' Velasquez', "
						     f"' Mckinney',' Christensen',' Stewart',' Hartley',' Cox',' Goodman',' Riley',' Vega', "
                             f"' Hughes',' Anderson',' Chang',' Bennett',' Hooper',' Montgomery',' Martin',' Sawyer', "
						     f"' Stephenson',' Kelley',' Moran',' Munoz',' Jenkins', ' Holmes',' Lee',' Lin',' Obrien',"
                             f"' Mccarthy'])[floor(random()*42+1)])as name_secondname, "
                             f"timestamp'1970-01-01'+random()*(timestamp'2001-01-01'-timestamp'1970-01-01') as "
                             f"dateofbirthday, (array ['A','B','C','D','T','E'])[floor(random()*6+1)]as category_of_driver_license "
                             f"from generate_series(1,{count})) as rnd")
        self.session.commit()
        print(f"Generate {count} records in table Owner car")
        print (color("After generation:",fore='B2B2FF'))
        rows_after=self.session.query(Owner_car).count()
        print(rows_after)
        return True

    def check(self):
        rows1 = self.session.query(Car).count()
        rows2 = self.session.query(Owner_car).count()
        if(rows1>rows2):
            print("You cannot generate car when people in database is less than cars")
            return False
        return True