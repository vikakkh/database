from entities import Product, Category, Producer
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
class DataBase:
    def connect(self):
        self.engine = create_engine('postgresql://postgres:vikakh2002@localhost:5432/storage_room')
        self.Session = sessionmaker(bind=self.engine)
        self.session= self.Session()

    # -----PRODUCT-----
    def get_product_by_id(self,id):
        product=self.session.query(Product).get(id)
        return product

    def insert_product(self,product):
        self.session.add(product)
        self.session.commit()
        return product.id_product

    def update_product(self,product):
        update_product = self.get_product_by_id(product.id_product)
        update_product.name_product = product.name_product
        update_product.price = product.price
        update_product.weight_product = product.weight_product
        update_product.shelf_life = product.shelf_life
        update_product.id_category = product.id_category
        update_product.product_balance = product.product_balance
        self.session.commit()
        return True

    def delete_product(self,id):
        delete_product=self.get_product_by_id(id)
        self.session.delete(delete_product)
        self.session.commit()
        return True

    # -----CATEGORY-----
    def get_category_by_id(self,id):
        category = self.session.query(Category).get(id)
        return  category

    def insert_category(self,category):
        self.session.add(category)
        self.session.commit()
        return category.id_category

    def update_category(self,category):
        update_category = self.get_category_by_id(category.id_category)
        update_category.name_category=category.name_category
        self.session.commit()
        return True

    def delete_category(self,id):
        delete_category=self.get_category_by_id(id)
        self.session.delete(delete_category)
        self.session.commit()
        return True

    # -----PRODUCER----
    def get_producer_by_id(self,id):
        producer = self.session.query(Producer).get(id)
        return  producer

    def insert_producer(self,producer):
        self.session.add(producer)
        self.session.commit()
        return producer.id_producer

    def update_producer(self,producer):
        update_producer = self.get_producer_by_id(producer.id_producer)
        update_producer.name_producer=producer.name_producer
        update_producer.adress=producer.adress
        update_producer.bank_details=producer.bank_details
        self.session.commit()
        return True

    def delete_producer(self,id):
        delete_producer=self.get_producer_by_id(id)
        self.session.delete(delete_producer)
        self.session.commit()
        return True