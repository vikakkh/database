from orm import DataBase
from view import View

def main():
    view = View()
    db = DataBase()
    db.connect()
    while True:
        inp = view.cui_enteties()
        if inp == "product":
            while True:
                choice = view.work_table()
                if choice == 1:
                    product_create_control(db, view)
                if choice == 2:
                    product_delete_control(db, view)
                if choice == 3:
                    product_update_control(db, view)
                if choice == 4:
                    product_get_control(db, view)
                if choice == 5:
                    break
        if inp == "category":
            while True:
                choice = view.work_table()
                if choice == 1:
                    category_create_control(db, view)
                if choice == 2:
                    category_delete_control(db, view)
                if choice == 3:
                    category_update_control(db, view)
                if choice == 4:
                    category_get_control(db, view)
                if choice == 5:
                    break
        if inp == "producer":
            while True:
                choice = view.work_table()
                if choice == 1:
                    producer_create_control(db, view)
                if choice == 2:
                    producer_delete_control(db, view)
                if choice == 3:
                    producer_update_control(db, view)
                if choice == 4:
                    producer_get_control(db, view)
                if choice == 5:
                    break
        if inp == "exit":
            break





#product
def product_get_control(db, view):
    id = view.get_id()
    product = db.get_product_by_id(id)
    if (product == False):
        return
    view.input_product(product)

def product_create_control(db, view):
    product = view.create_product()
    product.id = db.insert_product(product)
    view.input_product(product)

def product_delete_control(db, view):
    id = view.get_id()
    product = db.get_product_by_id(id)
    view.input_product(product)
    db.delete_product(id)

def product_update_control(db, view):
    id = view.get_id()
    product = db.get_product_by_id(id)
    view.input_product(product)
    prod = view.update_product(product)
    db.update_product(prod)
    view.input_product(prod)


# category
def category_get_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    if(category==False):
        return
    view.input_category(category)

def category_create_control(db, view):
    category = view.create_category()
    category.id = db.insert_category(category)
    print(category.id)
    view.input_category(category)

def category_delete_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    view.input_category(category)
    db.delete_category(id)

def category_update_control(db, view):
    id = view.get_id()
    category = db.get_category_by_id(id)
    view.input_category(category)
    cat = view.update_category(category)
    db.update_category(cat)
    view.input_category(cat)

#producer
def producer_get_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    if (producer == False):
        return
    view.input_producer(producer)

def producer_create_control(db, view):
    producer = view.create_producer()
    producer.id = db.insert_producer(producer)
    view.input_producer(producer)

def producer_delete_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    view.input_producer(producer)
    db.delete_producer(id)

def producer_update_control(db, view):
    id = view.get_id()
    producer = db.get_producer_by_id(id)
    view.input_producer(producer)
    pro = view.update_producer(producer)
    db.update_producer(pro)
    view.input_producer(pro)

main()