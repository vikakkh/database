import psycopg2
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy import Table
from sqlalchemy import ForeignKey

Base = declarative_base()

class Product(Base):
    __tablename__='product'
    id_product = Column(Integer, primary_key=True)
    name_product = Column(String)
    price = Column(Integer)
    weight_product = Column(Integer)
    shelf_life = Column(Integer)
    id_category= Column(Integer, ForeignKey('category.id_category'))
    _category=relationship("Category", backref="product")
    product_balance= Column(Integer)
    def __init__(self, name, price_pr, weight, shelf,category, balance):
        self.name_product = name
        self.price = price_pr
        self.weight_product = weight
        self.shelf_life = shelf
        self.id_category=category
        self.product_balance = balance

class Category(Base):
    __tablename__='category'
    id_category=Column(Integer,primary_key=True)
    name_category=Column(String)
    def __init__(self, name):
        self.name_category = name

# class Deliveries(Base):
#     __tablename__ = 'deliveries'
#     id_deliveries = Column(Integer,primary_key=True)
#     id_producer = Column(Integer,ForeignKey('producer.id_producer'))
#     _producer=relationship("Producer", backref="deliveries")
#     id_product = Column(Integer,ForeignKey('product.id_product'))
#     _product=relationship("Product", backref="deliveries")
#     date_of_delivery=Column(Date)
#     quanity = Column(Integer)
#     def __init__(self, producer, product, date, quantity_d):
#         self.id_producer = producer
#         self.id_product = product
#         self.date_of_delivery = date
#         self.quantity = quantity_d

class Producer(Base):
    __tablename__ ='producer'
    id_producer=Column(Integer, primary_key=True)
    name_producer=Column(String(60))
    adress=Column(String(60))
    bank_details=Column(Integer)
    def __init__(self, name, address, bank):
        self.id = id
        self.name_producer = name
        self.adress = address
        self.bank_details = bank
