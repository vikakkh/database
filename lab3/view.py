from entities import Product, Category, Producer

class View:
#menu
    def cui_enteties(self):
        while True:
            choice = input("Select table you want to work with:\nProduct \nCategory \nProducer "
                           "\n Exit \nChoice: ")
            if (choice.isdigit() != False):
                print("Write correct name of table!")
                continue
            break
        return str(choice)

    def work_table (self):
        while True:
            choice = input("Enter number: \n1.Create entity \n2. Remove entity \n3. Update entity "
                           "\n4. Input by id \n5. Exit \nChoice: ")
            if(choice.isdigit()!= True):
                print("Incorrect enter. Please enter correct number")
                continue
            break
        return int(choice)
#functions
    def get_id(self):
        while(True):
            inp=(input("Enter entity id: "))
            if(inp.isdigit() != True):
                print("Incorrect enter! ")
                continue
            break
        return int(inp)
#tables
    def input_product (self, product):
        print("id: ", product.id_product)
        print("Name: ", product.name_product)
        print("Price: ", product.price)
        print("Weight: ", product.weight_product)
        print("Shelf life: ", product.shelf_life)
        print("Id category: ", product.id_category)
        print("Balance: ", product.product_balance)

    def input_all_product (self, products):
        for product in products:
            print("id: ", product.id)
            print("Name: ", product.name)
            print("Price: ", product.price)
            print("Weight: ", product.weight)
            print("Shelf life: ", product.shelf)
            print("Id category: ", product.id_category)
            print("Balance: ", product.balance)

    def input_category(self, category):
        print("Id: ", category.id_category)
        print("Name: ", category.name_category)



    def input_producer(self, producer):
        print("Id: ", producer.id_producer)
        print("Name: ", producer.name_producer)
        print("Address: ", producer.adress)
        print("Bank details: ", producer.bank_details)

    def input_all_producer(self, producers):
        for producer in producers:
            print("Id: ", producer.id)
            print("Name: ", producer.name)
            print("Address: ", producer.adress)
            print("Bank details: ", producer.bank)

    def input_all_deliveries(self,deliveries):
        for delivery in deliveries:
            print("Id: ", delivery.id)
            print("Producer Id: ", delivery.producer)
            print("Product Id: ", delivery.product)
            print("Date of delivery: ", delivery.date)
            print("Quantity: ", delivery.quantity)


#product
    def create_product(self):
        while(True):
            name = input("Enter name of product: ").lstrip()
            name.rstrip()
            price = input("Enter price: ")
            weight = input("Enter weight of product: ")
            shelf = input("Enter shelf life of product: ")
            category_id = input("Enter the category product belongs to: ")
            balance = input("Enter balance: ")
            break
        product = Product(name, price, weight, shelf, category_id, balance)
        return product

    def update_product(self, product):
        while(True):
            num = input("Enter number to change: \n1. Name \n2. Price \n3. Weight"
                    "\n4. Shelf life \n5.Category \n6. Balance"
                        "\n--:")
            if (num.isdigit()!=True):
                print("Enter correct input")
                continue
            break
        if (int(num)==1):
            name = input("Enter new Name: ").lstrip()
            name.rstrip()
            product.name_product = name
        if (int(num)==2):
            while(True):
                price = input("Enter new price: ")
                if(price.isdigit()!= True):
                    print("Input number! ")
                    continue
                break
            product.price = price
        if (int(num) == 3):
            while (True):
                weight = input("Enter new weight: ")
                if (weight.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.weight_product = weight
        if (int(num) == 4):
            while (True):
                shelf = input("Enter new shelf life of product: ")
                if (shelf.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.shelf_life = shelf
        if (int(num) == 5):
            while (True):
                category = input("change category: ")
                if (category.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.id_category = category
        if (int(num) == 6):
            while (True):
                bal = input("Enter new balance: ")
                if (bal.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            product.product_balance = bal
        return product

#category
    def create_category(self):
        name = input("Enter new category: ").lstrip()
        name.rstrip()
        category = Category(name)
        return category

    def update_category(self, category):
        name = input("Enter new Name: ").lstrip()
        name.rstrip()
        category.name_category = name
        return category

#producer
    def create_producer(self):
        name = input("Enter name of producer: ").lstrip()
        name.rstrip()
        adress = input("Enter the address: ").lstrip()
        adress.rstrip()
        bank  = input ("Enter bank details: ")
        producer = Producer(name, adress, bank)
        return producer

    def update_producer(self, producer):
        while(True):
            num = input("Enter number to change: \n1. Name \n2. Address \n3. Bank details \nChoice: ")
            if (num.isdigit()!=True):
                print("Enter correct input")
                continue
            break
        if (int(num)==1):
            name = input("Enter new Name: ").lstrip()
            name.rstrip()
            producer.name_producer = name
        if(int(num)==2):
            address = input("Enter new address: ").lstrip()
            address.rstrip()
            producer.adress = address
        if (int(num) == 3):
            while (True):
                bank = input("Enter correct bank details: ")
                if (bank.isdigit() != True):
                    print("Input number! ")
                    continue
                break
            producer.bank_details = bank
        return producer
